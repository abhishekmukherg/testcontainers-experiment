package testcontainers.experiment

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.testcontainers.containers.GenericContainer
import redis.clients.jedis.Jedis
import kotlin.test.assertEquals

class KGenericContainer(imageName: String) : GenericContainer<KGenericContainer>(imageName)

class RedisBackedCacheIntTest {

    var underTest: Jedis? = null;

    // rule {
    @Rule
    @JvmField
    var redis = KGenericContainer("redis:5.0.3-alpine").withExposedPorts(6379)
    // }


    @Before
    fun setUp() {
        val address = redis.getHost();
        val port = redis.getFirstMappedPort();

        // Now we have an address and port for Redis, no matter where it is running
        underTest = Jedis(address, port);
    }

    @Test
    fun testSimplePutAndGet() {
        underTest!!.set("test", "example");

        val retrieved = underTest!!.get("test");
        assertEquals("example", retrieved);
    }
}
